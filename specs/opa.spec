%global namespace github.com/open-policy-agent/%{name}
%global go_version 1.13.10

Name:             opa
Version:          0.26.0
Release:          1%{?dist}.harbottle
Summary:          Open Policy Agent
Group:            Applications/System
License:          Apache-2.0
Url:              https://github.com/open-policy-agent/%{name}
Source0:          %{url}/archive/v%{version}.tar.gz
Source1:          opa.sysconfig
Source2:          opa.yaml
Source3:          opa.service
%if 0%{?rhel} == 8
Source4:          https://dl.google.com/go/go%{go_version}.linux-amd64.tar.gz
%else
BuildRequires:    golang >= 1.13
%endif
BuildRequires:    make which systemd-units zip
Requires(pre):    shadow-utils
Requires(post):   systemd-units
Requires(preun):  systemd-units
Requires(postun): systemd-units

%description
The Open Policy Agent (OPA) is an open source, general-purpose policy engine
that enables unified, context-aware policy enforcement across the entire stack.

%prep
%setup -q
%if 0%{?rhel} == 8
%setup -q -T -D -a 4 -n %{name}-%{version}
%endif

%build
%define debug_package %{nil}
export GOPROXY=https://proxy.golang.org
export GOPATH=$PWD
%if 0%{?rhel} == 8
export GOROOT=$PWD/go
export PATH=$GOPATH/bin:$GOROOT/bin:$PATH
%else
export PATH=${PATH}:${GOPATH}/bin
%endif
mkdir -p src/%{namespace}
shopt -s extglob dotglob
%if 0%{?rhel} == 8
mv !(src|go) src/%{namespace}
%else
mv !(src) src/%{namespace}
%endif
shopt -u extglob dotglob
pushd src/%{namespace}
make build
popd

%install
install -d -m 0755 $RPM_BUILD_ROOT%{_bindir}
install -d -m 0770 $RPM_BUILD_ROOT%{_sharedstatedir}/%{name}
install -d -m 0750 $RPM_BUILD_ROOT%{_sysconfdir}/sysconfig
install -d -m 0750 $RPM_BUILD_ROOT%{_sysconfdir}/%{name}
install -d -m 0755 $RPM_BUILD_ROOT%{_unitdir}
install -m 0755 src/%{namespace}/%{name}_linux_amd64 $RPM_BUILD_ROOT%{_bindir}/%{name}
install -m 0640 %{SOURCE1} $RPM_BUILD_ROOT%{_sysconfdir}/sysconfig/%{name}
install -m 0640 %{SOURCE2} $RPM_BUILD_ROOT%{_sysconfdir}/%{name}.yaml
install -m 0640 %{SOURCE3} $RPM_BUILD_ROOT%{_unitdir}/%{name}.service

%pre
getent group %{name} >/dev/null || groupadd -f -r %{name}
getent passwd %{name} >/dev/null || useradd -r -g %{name} -d %{_sharedstatedir}/%{name} -s /sbin/nologin -c "Open Policy Agent" %{name}
exit 0

%post
%systemd_post %{name}.service

%preun
%systemd_preun %{name}.service

%postun
%systemd_postun_with_restart %{name}.service

%files
%license src/%{namespace}/LICENSE
%doc src/%{namespace}/{*.md,*.pdf}
%{_bindir}/%{name}
%config(noreplace) %attr(-,root,%{name}) %{_sysconfdir}/sysconfig/%{name}
%config(noreplace) %attr(-,root,%{name}) %{_sysconfdir}/%{name}
%config(noreplace) %attr(-,root,%{name}) %{_sysconfdir}/%{name}.yaml
%attr(-,%{name},%{name}) %{_sharedstatedir}/%{name}
%attr(0644,root,root) %{_unitdir}/%{name}.service

%changelog
* Wed Jan 20 2021 - harbottle@room3d3.com - 0.26.0-1
  - Bump version

* Tue Dec 08 2020 - harbottle@room3d3.com - 0.25.2-1
  - Bump version

* Sat Dec 05 2020 - harbottle@room3d3.com - 0.25.1-1
  - Bump version

* Thu Dec 03 2020 - harbottle@room3d3.com - 0.25.0-1
  - Bump version

* Fri Oct 16 2020 - harbottle@room3d3.com - 0.24.0-1
  - Bump version

* Mon Aug 24 2020 - harbottle@room3d3.com - 0.23.2-1
  - Bump version

* Thu Aug 20 2020 - harbottle@room3d3.com - 0.23.1-1
  - Bump version

* Fri Jul 24 2020 - harbottle@room3d3.com - 0.22.0-1
  - Bump version

* Thu Jul 09 2020 - harbottle@room3d3.com - 0.21.1-1
  - Bump version

* Tue Jun 16 2020 - harbottle@room3d3.com - 0.21.0-1
  - Bump version

* Mon Jun 01 2020 - harbottle@room3d3.com - 0.20.5-1
  - Bump version

* Sat May 23 2020 - harbottle@room3d3.com - 0.20.4-2
  - Add systemd service

* Fri May 22 2020 - harbottle@room3d3.com - 0.20.4-1
  - Bump version

* Fri May 22 2020 - harbottle@room3d3.com - 0.20.3-1
  - Bump version

* Thu May 21 2020 - harbottle@room3d3.com - 0.20.2-1
  - Bump version

* Thu May 21 2020 - harbottle@room3d3.com - 0.20.1-1
  - Bump version

* Thu May 21 2020 - harbottle@room3d3.com - 0.20.0-1
  - Bump version

* Tue Apr 28 2020 - harbottle@room3d3.com - 0.19.2-1
  - Bump version

* Wed Apr 15 2020 - harbottle@room3d3.com - 0.19.1-1
  - Initial package
