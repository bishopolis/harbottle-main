%global owner ahmetb
%global repo %{name}
%global host github.com
%global archive v%{version}.tar.gz
%global dir %{repo}-%{version}

Name:     kubectx
Version:  0.6.3
Release:  1.el7.harbottle
Summary:  Switch faster between clusters and namespaces in kubectl
Group:    Applications/System
License:  Apache 2.0
Url:      https://%{host}/%{owner}/%{repo}
Source0:  %{url}/archive/%{archive}
Provides: kubectx
Provides: kubens
Requires: bash-completion

%description
Switch faster between clusters and namespaces in kubectl.

kubectx is a utility to manage and switch between kubectl contexts.

kubens is a utility to switch between Kubernetes namespaces.

%prep
%setup -q -n %{dir}

%install
install -d -m 755 $RPM_BUILD_ROOT%{_bindir}
install -d -m 755 $RPM_BUILD_ROOT%{_datadir}/bash-completion/completions
install -m 755 kubectx $RPM_BUILD_ROOT%{_bindir}
install -m 755 kubens $RPM_BUILD_ROOT%{_bindir}
install -m 644 completion/kubectx.bash $RPM_BUILD_ROOT%{_datadir}/bash-completion/completions/kubectx
install -m 644 completion/kubens.bash $RPM_BUILD_ROOT%{_datadir}/bash-completion/completions/kubens

%files
%license LICENSE
%doc CONTRIBUTING.md README.md
%{_bindir}/kubectx
%{_bindir}/kubens
%{_datadir}/bash-completion/completions/kubectx
%{_datadir}/bash-completion/completions/kubens

%changelog
* Tue Apr 02 2019 - harbottle@room3d3.com - 0.6.3-1
  - Initial package
