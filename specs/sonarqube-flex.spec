%define __jar_repack 0

Name:     sonarqube-flex
Version:  2.6.0.2294
Release:  1%{?dist}.harbottle
Summary:  SonarQube Flex plugin
Group:    Applications/System
License:  LGPL-3.0
Source0:  https://binaries.sonarsource.com/Distribution/sonar-flex-plugin/sonar-flex-plugin-%{version}.jar
Autoprov: no

%description
Flex plugin for SonarQube.

%prep

%install
install -d -m 755 $RPM_BUILD_ROOT%{_var}/lib/sonarqube/extensions/plugins
mv %{SOURCE0} $RPM_BUILD_ROOT%{_var}/lib/sonarqube/extensions/plugins

%pre
rm -f %{_var}/lib/sonarqube/extensions/plugins/sonar-flex-plugin-*.jar
getent group sonarqube >/dev/null || groupadd -f -r sonarqube
getent passwd sonarqube >/dev/null || useradd -r -g sonarqube -d /usr/share/sonarqube -s /sbin/nologin -c "SonarQube user" sonarqube
exit 0

%files
%attr(0644,sonarqube,sonarqube) %{_var}/lib/sonarqube/extensions/plugins/sonar-flex-plugin-%{version}.jar

%changelog
* Mon Oct 05 2020 - harbottle@room3d3.com - 2.6.0.2294-1
  - Bump version

* Tue Dec 10 2019 - harbottle@room3d3.com - 2.5.1.1831-3
  - Spec file changes for el8

* Sun Jul 21 2019 - harbottle@room3d3.com - 2.5.1.1831-2
  - Fix plugin ownership

* Sun Jul 21 2019 - harbottle@room3d3.com - 2.5.1.1831-1
  - Initial package
