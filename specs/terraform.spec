Name:    terraform
Version: 0.14.7
Release: 1%{?dist}.harbottle
Summary: Write, Plan, and Create Infrastructure as Code.
Group:   Applications/System
License: MPL-2.0
URL:     https://terraform.io/
Source0: https://github.com/hashicorp/%{name}/archive/v%{version}.tar.gz
Source1: https://releases.hashicorp.com/%{name}/%{version}/%{name}_%{version}_linux_amd64.zip

%description
Terraform enables you to safely and predictably create, change, and improve
production infrastructure. It is an open source tool that codifies APIs into
declarative configuration files that can be shared amongst team members, treated
as code, edited, reviewed, and versioned.

%prep
%setup -q
rm -rf terraform
%setup -q -T -D -a 1

%install
install -d -m 755 $RPM_BUILD_ROOT%{_bindir}
install -m 0755 %{name} $RPM_BUILD_ROOT%{_bindir}

%files
%license LICENSE
%doc *.md
%{_bindir}/%{name}

%changelog
* Wed Feb 17 2021 - harbottle@room3d3.com - 0.14.7-1
  - Bump version

* Thu Feb 04 2021 - harbottle@room3d3.com - 0.14.6-1
  - Bump version

* Wed Jan 20 2021 - harbottle@room3d3.com - 0.14.5-1
  - Bump version

* Wed Jan 06 2021 - harbottle@room3d3.com - 0.14.4-1
  - Bump version

* Fri Dec 18 2020 - harbottle@room3d3.com - 0.14.3-1
  - Bump version

* Tue Dec 08 2020 - harbottle@room3d3.com - 0.14.2-1
  - Bump version

* Tue Dec 08 2020 - harbottle@room3d3.com - 0.14.1-1
  - Bump version

* Wed Dec 02 2020 - harbottle@room3d3.com - 0.14.0-1
  - Bump version

* Wed Oct 21 2020 - harbottle@room3d3.com - 0.13.5-1
  - Bump version

* Wed Sep 30 2020 - harbottle@room3d3.com - 0.13.4-1
  - Bump version

* Wed Sep 16 2020 - harbottle@room3d3.com - 0.13.3-1
  - Bump version

* Wed Sep 02 2020 - harbottle@room3d3.com - 0.13.2-1
  - Bump version

* Wed Aug 26 2020 - harbottle@room3d3.com - 0.13.1-1
  - Bump version

* Thu Aug 20 2020 - harbottle@room3d3.com - 0.13.0-1
  - Fix build
  - Bump version

* Fri Jul 24 2020 - harbottle@room3d3.com - 0.12.29-1
  - Bump version

* Thu Jun 25 2020 - harbottle@room3d3.com - 0.12.28-1
  - Bump version

* Wed Jun 24 2020 - harbottle@room3d3.com - 0.12.27-1
  - Bump version

* Wed May 27 2020 - harbottle@room3d3.com - 0.12.26-1
  - Bump version

* Wed May 13 2020 - harbottle@room3d3.com - 0.12.25-1
  - Bump version

* Thu Mar 19 2020 - harbottle@room3d3.com - 0.12.24-1
  - Bump version

* Thu Mar 05 2020 - harbottle@room3d3.com - 0.12.23-1
  - Bump version

* Thu Mar 05 2020 - harbottle@room3d3.com - 0.12.22-1
  - Bump version

* Sat Feb 29 2020 - harbottle@room3d3.com - 0.12.21-1
  - Bump version

* Wed Jan 22 2020 - harbottle@room3d3.com - 0.12.20-1
  - Bump version

* Sat Jan 18 2020 - harbottle@room3d3.com - 0.12.19-2
  - Build for el8
  - Tidy spec

* Wed Jan 08 2020 - harbottle@room3d3.com - 0.12.19-1
  - Bump version

* Thu Dec 19 2019 - harbottle@room3d3.com - 0.12.18-1
  - Bump version

* Mon Dec 02 2019 - harbottle@room3d3.com - 0.12.17-1
  - Bump version

* Tue Nov 19 2019 - harbottle@room3d3.com - 0.12.16-1
  - Bump version

* Thu Nov 14 2019 - harbottle@room3d3.com - 0.12.15-1
  - Bump version

* Wed Nov 13 2019 - harbottle@room3d3.com - 0.12.14-1
  - Bump version

* Thu Oct 31 2019 - harbottle@room3d3.com - 0.12.13-1
  - Bump version

* Fri Oct 18 2019 - harbottle@room3d3.com - 0.12.12-1
  - Bump version

* Thu Oct 17 2019 - harbottle@room3d3.com - 0.12.11-1
  - Bump version

* Mon Oct 07 2019 - harbottle@room3d3.com - 0.12.10-1
  - Bump version

* Tue Sep 17 2019 - harbottle@room3d3.com - 0.12.9-1
  - Bump version

* Wed Sep 04 2019 - harbottle@room3d3.com - 0.12.8-1
  - Bump version

* Thu Aug 22 2019 - harbottle@room3d3.com - 0.12.7-1
  - Bump version

* Wed Jul 31 2019 - harbottle@room3d3.com - 0.12.6-1
  - Bump version

* Thu Jul 18 2019 - harbottle@room3d3.com - 0.12.5-1
  - Bump version

* Thu Jul 11 2019 - harbottle@room3d3.com - 0.12.4-1
  - Bump version

* Mon Jun 24 2019 - harbottle@room3d3.com - 0.12.3-1
  - Bump version

* Wed Jun 12 2019 - harbottle@room3d3.com - 0.12.2-1
  - Bump version

* Wed Jun 05 2019 - harbottle@room3d3.com - 0.12.1-1
  - Bump version

* Mon Jun 03 2019 - harbottle@room3d3.com - 0.12.0-1
  - Bump version

* Mon Mar 11 2019 - harbottle@room3d3.com - 0.11.13-1
  - Bump version

* Fri Mar 08 2019 - harbottle@room3d3.com - 0.11.12-1
  - Bump version

* Sat Feb 02 2019 - harbottle@room3d3.com - 0.11.11-3
  - Build from source

* Sat Jan 26 2019 - harbottle@room3d3.com - 0.11.11-2
  - Use remote license file

* Wed Jan 02 2019 - harbottle@room3d3.com - 0.11.11-1
  - Bump version

* Mon Oct 15 2018 - Richard Grainger <grainger@gmail.com> - 0.11.8-1
- Bump version
* Mon Apr 30 2018 - Richard Grainger <grainger@gmail.com> - 0.11.7-1
- Bump version
* Fri Apr 06 2018 - Richard Grainger <grainger@gmail.com> - 0.11.6-1
- Bump version
* Sat Mar 03 2018 - Richard Grainger <grainger@gmail.com> - 0.11.3-1
- Bump version
* Mon Dec 04 2017 - Richard Grainger <grainger@gmail.com> - 0.11.1-1
- Bump version
* Mon Nov 20 2017 - Richard Grainger <grainger@gmail.com> - 0.11.0-1
- Bump version
* Wed Nov 01 2017 - Richard Grainger <grainger@gmail.com> - 0.10.8-1
- Initial spec, based on
  https://github.com/phrawzty/terraform-rpm/blob/master/terraform.spec
