Name:    mmctl
Version: 5.32.0
Release: 1%{?dist}.harbottle
Summary: A remote CLI tool for Mattermost
Group:   Applications/System
License: Apache-2.0
Url:     https://github.com/mattermost/%{name}
Source0: %{url}/archive/v%{version}.tar.gz
Source1: %{url}/releases/download/v%{version}/linux_amd64.tar

%description
A remote CLI tool for Mattermost: the Open Source, self-hosted
Slack-alternative.

%prep
%setup -q
%setup -q -T -D -a 1

%install
install -d -m 755 $RPM_BUILD_ROOT%{_bindir}
install -m 755 %{name} $RPM_BUILD_ROOT%{_bindir}

%files
%license LICENSE.txt
%doc *.md docs/*
%{_bindir}/%{name}

%changelog
* Thu Jan 28 2021 - harbottle@room3d3.com - 5.32.0-1
  - Bump version

* Tue Dec 22 2020 - harbottle@room3d3.com - 5.31.0-1
  - Bump version

* Thu Dec 03 2020 - harbottle@room3d3.com - 5.30.0-1
  - Bump version

* Mon Oct 19 2020 - harbottle@room3d3.com - 5.29.0-1
  - Update version
  - Package from official binary
  - Include license

* Sun Mar 15 2020 - harbottle@room3d3.com - 5.21.0-1
  - Initial package
