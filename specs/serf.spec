%global owner hashicorp
%global host github.com
%global archive v%{version}.tar.gz
%global dir %{name}-%{version}
%global namespace %{host}/%{owner}/%{name}

%global confdir %{_sysconfdir}/%{name}.d

Name:             serf
Version:          0.9.5
Release:          2%{?dist}.harbottle
Summary:          Serf is a decentralized solution for service discovery and orchestration
Group:            Applications/System
License:          MPLv2.0
URL:              https://serf.io/
Source0:          https://%{namespace}/archive/%{archive}
Source1:          %{name}.service
BuildRequires:    git golang make systemd-units which zip
Requires(pre):    shadow-utils
Requires(post):   systemd-units
Requires(preun):  systemd-units
Requires(postun): systemd-units

%description
Serf is a decentralized solution for service discovery and orchestration that is
lightweight, highly available, and fault tolerant.

An efficient and lightweight gossip protocol is used to communicate with other
nodes. Serf can detect node failures and notify the rest of the cluster. An
event system is built on top of Serf, letting you use Serf's gossip protocol to
propagate events such as deploys, configuration changes, etc. Serf is completely
masterless with no single point of failure.

%prep
%setup -q -n %{dir}

%build
%define debug_package %{nil}
export GOFLAGS="${GOPATH} -modcacherw"
export GOPATH=$PWD
export PATH=${PATH}:${GOPATH}/bin
export XC_ARCH=amd64
export XC_OS=linux
mkdir -p src/%{namespace}
shopt -s extglob dotglob
mv !(src) src/%{namespace}
shopt -u extglob dotglob
pushd src/%{namespace}
git init && git config user.name %{name} && git config user.email %{name}@%{owner}
git add . && git commit -m %{name} && git tag v%{version}
make get-tools && make bin
popd

%install
install -d -m 755 $RPM_BUILD_ROOT%{_bindir}
install -d -m 755 $RPM_BUILD_ROOT%{confdir}
install -d -m 755 $RPM_BUILD_ROOT%{_unitdir}

install -m 0755 src/%{namespace}/bin/%{name} $RPM_BUILD_ROOT%{_bindir}
install -m 0644 %{SOURCE1} $RPM_BUILD_ROOT%{_unitdir}

%pre
getent group %{name} >/dev/null || groupadd -f -r %{name}
getent passwd %{name} >/dev/null || useradd -r -g %{name} -d %{confdir} -s /sbin/nologin -c "%{name} service user" %{name}
exit 0

%post
%systemd_post %{name}.service

%preun
%systemd_preun %{name}.service

%postun
%systemd_postun_with_restart %{name}.service

%files
%license src/%{namespace}/LICENSE
%doc src/%{namespace}/{CHANGELOG.md,README.md}
%{_bindir}/%{name}
%attr(-,%{name},%{name}) %config(noreplace) %{confdir}
%{_unitdir}/%{name}.service

%changelog
* Sat Feb 20 2021 - bishopolis@gmail.com - 0.9.5-2
  - set the go tree RW to retain ability for repeat builds
* Wed Oct 07 2020 - harbottle@room3d3.com - 0.9.5-1
  - Bump version

* Thu Aug 20 2020 - harbottle@room3d3.com - 0.9.4-1
  - Bump version

* Fri Jul 24 2020 - harbottle@room3d3.com - 0.9.3-1
  - Bump version

* Tue May 05 2020 - harbottle@room3d3.com - 0.9.2-1
  - Bump version

* Mon May 04 2020 - harbottle@room3d3.com - 0.9.1-1
  - Bump version

* Mon Mar 09 2020 - harbottle@room3d3.com - 0.9.0-1
  - Bump version

* Wed Oct 02 2019 - harbottle@room3d3.com - 0.8.5-1
  - Bump version

* Thu Sep 19 2019 - harbottle@room3d3.com - 0.8.4-1
  - Bump version

* Wed Apr 10 2019 - harbottle@room3d3.com - 0.8.3-1
  - Bump version

* Sun Feb 03 2019 - harbottle@room3d3.com - 0.8.2-1
  - Initial package
