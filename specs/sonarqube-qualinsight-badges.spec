%define __jar_repack 0

Name:     sonarqube-qualinsight-badges
Version:  8.2.0.32929
Release:  1%{?dist}.harbottle
Summary:  SonarQube qualinsight badges plugin
Group:    Applications/System
License:  LGPL-3.0
URL:      https://github.com/maxiko/qualinsight-plugins-sonarqube-badges
Source0:  %{url}/releases/download/v%{version}/qualinsight-sonarqube-badges-%{version}.jar
Autoprov: no

%description
Qualinsight badges plugin for SonarQube.

%prep

%install
install -d -m 755 $RPM_BUILD_ROOT%{_var}/lib/sonarqube/extensions/plugins
mv %{SOURCE0} $RPM_BUILD_ROOT%{_var}/lib/sonarqube/extensions/plugins

%pre
rm -f %{_var}/lib/sonarqube/extensions/plugins/qualinsight-sonarqube-badges-*.jar
getent group sonarqube >/dev/null || groupadd -f -r sonarqube
getent passwd sonarqube >/dev/null || useradd -r -g sonarqube -d /usr/share/sonarqube -s /sbin/nologin -c "SonarQube user" sonarqube
exit 0

%files
%attr(0644,sonarqube,sonarqube) %{_var}/lib/sonarqube/extensions/plugins/qualinsight-sonarqube-badges-%{version}.jar

%changelog
* Thu Mar 12 2020 - harbottle@room3d3.com - 8.2.0.32929-1
  - Bump version
  - Fix build

* Fri Jan 24 2020 - harbottle@room3d3.com - 7.9.2-1
  - Initial package
