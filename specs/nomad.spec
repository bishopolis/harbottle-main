%global confdir %{_sysconfdir}/%{name}.d
%global datadir %{_var}/lib/%{name}/

Name:             nomad
Version:          1.0.3
Release:          1%{?dist}.harbottle
Summary:          Nomad is an easy-to-use, flexible, and performant workload orchestrator
Group:            Applications/System
License:          MPLv2.0
URL:              https://www.nomadproject.io/
Source0:          https://releases.hashicorp.com/%{name}/%{version}/%{name}_%{version}_linux_amd64.zip
Source1:          %{name}.hcl
Source2:          client.hcl
Source3:          %{name}.service
Source4:          https://raw.githubusercontent.com/hashicorp/%{name}/v%{version}/CHANGELOG.md
Source5:          https://raw.githubusercontent.com/hashicorp/%{name}/v%{version}/LICENSE
Source6:          https://raw.githubusercontent.com/hashicorp/%{name}/v%{version}/README.md
Requires(post):   systemd-units
Requires(preun):  systemd-units
Requires(postun): systemd-units

%description
Nomad is an easy-to-use, flexible, and performant workload orchestrator that can
deploy a mix of microservice, batch, containerized, and non-containerized
applications. Nomad is easy to operate and scale and has native Consul and Vault
integrations.

%prep
%setup -q -c

%install
mv %{SOURCE4} .
mv %{SOURCE5} .
mv %{SOURCE6} .

install -d -m 755 $RPM_BUILD_ROOT%{_bindir}
install -d -m 755 $RPM_BUILD_ROOT%{confdir}
install -d -m 755 $RPM_BUILD_ROOT%{datadir}
install -d -m 755 $RPM_BUILD_ROOT%{_unitdir}

install -m 0755 %{name} $RPM_BUILD_ROOT%{_bindir}
install -m 0640 %{SOURCE1} $RPM_BUILD_ROOT%{confdir}
install -m 0640 %{SOURCE2} $RPM_BUILD_ROOT%{confdir}
install -m 0644 %{SOURCE3} $RPM_BUILD_ROOT%{_unitdir}

%post
%systemd_post %{name}.service

%preun
%systemd_preun %{name}.service

%postun
%systemd_postun_with_restart %{name}.service

%files
%license LICENSE
%doc CHANGELOG.md README.md
%{_bindir}/%{name}
%config(noreplace) %{confdir}
%{datadir}
%{_unitdir}/%{name}.service

%changelog
* Fri Jan 29 2021 - harbottle@room3d3.com - 1.0.3-1
  - Bump version

* Thu Jan 14 2021 - harbottle@room3d3.com - 1.0.2-1
  - Bump version

* Fri Dec 18 2020 - harbottle@room3d3.com - 1.0.1-1
  - Bump version

* Tue Dec 08 2020 - harbottle@room3d3.com - 1.0.0-1
  - Bump version

* Fri Nov 20 2020 - harbottle@room3d3.com - 0.12.9-1
  - Bump version

* Wed Nov 11 2020 - harbottle@room3d3.com - 0.12.8-1
  - Bump version

* Fri Oct 23 2020 - harbottle@room3d3.com - 0.12.7-1
  - Bump version

* Wed Oct 21 2020 - harbottle@room3d3.com - 0.12.6-1
  - Bump version

* Sat Sep 19 2020 - harbottle@room3d3.com - 0.12.5-1
  - Bump version

* Thu Sep 10 2020 - harbottle@room3d3.com - 0.12.4-1
  - Bump version

* Thu Aug 13 2020 - harbottle@room3d3.com - 0.12.3-1
  - Bump version

* Wed Aug 12 2020 - harbottle@room3d3.com - 0.12.2-1
  - Bump version

* Fri Jul 24 2020 - harbottle@room3d3.com - 0.12.1-1
  - Bump version

* Thu Jul 09 2020 - harbottle@room3d3.com - 0.12.0-1
  - Bump version

* Fri Jun 05 2020 - harbottle@room3d3.com - 0.11.3-1
  - Bump version

* Thu May 14 2020 - harbottle@room3d3.com - 0.11.2-1
  - Bump version

* Wed Apr 22 2020 - harbottle@room3d3.com - 0.11.1-1
  - Bump version

* Wed Apr 08 2020 - harbottle@room3d3.com - 0.11.0-1
  - Bump version

* Wed Mar 25 2020 - harbottle@room3d3.com - 0.10.5-1
  - Bump version

* Sat Feb 29 2020 - harbottle@room3d3.com - 0.10.4-1
  - Bump version

* Thu Jan 30 2020 - harbottle@room3d3.com - 0.10.3-1
  - Bump version

* Fri Dec 06 2019 - harbottle@room3d3.com - 0.10.2-1
  - Bump version

* Mon Nov 04 2019 - harbottle@room3d3.com - 0.10.1-1
  - Bump version

* Tue Oct 22 2019 - harbottle@room3d3.com - 0.10.0-1
  - Bump version

* Tue Oct 08 2019 - harbottle@room3d3.com - 0.9.6-1
  - Bump version

* Thu Aug 22 2019 - harbottle@room3d3.com - 0.9.5-1
  - Bump version

* Tue Jul 30 2019 - harbottle@room3d3.com - 0.9.4-1
  - Bump version

* Wed Jun 12 2019 - harbottle@room3d3.com - 0.9.3-1
  - Bump version

* Wed Jun 12 2019 - harbottle@room3d3.com - 0.9.2-1
  - Initial package

