%global homedir %{_libdir}/tomcat10
%global sslver 1.1.1a

Name:           tomcat10-native
Version:        1.2.26
Release:        1%{?dist}.harbottle
Summary:        Tomcat 10 native library
Group:          Applications/System
License:        Apache-2.0
URL:            https://tomcat.apache.org/tomcat-10.0-doc/apr.html
Source0:        http://www.apache.org/dist/tomcat/tomcat-connectors/native/%{version}/source/tomcat-native-%{version}-src.tar.gz
Source1:        https://www.openssl.org/source/openssl-%{sslver}.tar.gz
Source2:        tomcat10-native-x86_64.conf
BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
BuildRequires:  java-devel
BuildRequires:  jpackage-utils
BuildRequires:  apr-devel >= 1.2.1
BuildRequires:  gcc
BuildRequires:  make
BuildRequires:  perl
BuildRequires:  pcre-devel
BuildRequires:  zlib-devel
BuildRequires:  wget
Provides:       tcnative10 = %{version}-%{release}

%description
Tomcat can use the Apache Portable Runtime to provide superior
scalability, performance, and better integration with native server
technologies.  The Apache Portable Runtime is a highly portable library
that is at the heart of Apache HTTP Server 2.x.  APR has many uses,
including access to advanced IO functionality (such as sendfile, epoll
and OpenSSL), OS level functionality (random number generation, system
status, etc), and native process handling (shared memory, NT pipes and
Unix sockets).  This package contains the Tomcat native library which
provides support for using APR in Tomcat.

%prep
%setup -q -c -n tomcat-native -a 0 -a 1
pushd tomcat-native-%{version}-src
 f=CHANGELOG.txt ; iconv -f iso-8859-1 -t utf-8 $f > $f.utf8 ; mv $f.utf8 $f
popd

%build
pushd openssl-%{sslver}
  ./config --prefix=%{_tmppath}/openssl --openssldir=%{_tmppath}/etc/ssl --libdir=lib no-shared zlib-dynamic -fPIC
  make
  make install_sw
popd

cd tomcat-native-%{version}-src/native
%configure \
    --with-apr=%{_bindir}/apr-1-config \
    --with-java-home=%{java_home} \
    --with-ssl=%{_tmppath}/openssl \
    --libdir=/lib
make %{?_smp_mflags}

%install
rm -rf $RPM_BUILD_ROOT
make -C tomcat-native-%{version}-src/native install DESTDIR=$RPM_BUILD_ROOT%{homedir}
# Perhaps a devel package sometime?  Not for now; no headers are installed.
rm -f $RPM_BUILD_ROOT%{homedir}/lib/libtcnative*.*a
rm -rf $RPM_BUILD_ROOT%{homedir}/lib/pkgconfig
rm -rf $RPM_BUILD_ROOT%{homedir}/usr
install -d -m 755 $RPM_BUILD_ROOT%{_sysconfdir}/ld.so.conf.d
install -m 644 %{SOURCE2} $RPM_BUILD_ROOT%{_sysconfdir}/ld.so.conf.d/%{name}-x86_64.conf

%clean
rm -rf $RPM_BUILD_ROOT

%post -p /sbin/ldconfig

%postun -p /sbin/ldconfig

%files

%defattr(-,root,root,-)
%{!?_licensedir:%global license %%doc}
%license tomcat-native-%{version}-src/LICENSE tomcat-native-%{version}-src/NOTICE
%doc tomcat-native-%{version}-src/CHANGELOG.txt tomcat-native-%{version}-src/TODO.txt
# Note: unversioned *.so needed here due to how Tomcat loads the lib :(
%{homedir}/lib/libtcnative*.so*
%{_sysconfdir}/ld.so.conf.d

%changelog
* Mon Feb 08 2021 - ciro.deluca@autistici.org - 10.0.2-1
  - Update version

* Sat Jan 30 2021 - ciro.deluca@autistici.org - 10.0.0-1
  - Initial packaging
