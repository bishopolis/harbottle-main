Name:           puppet-release
Version:        1.0.0
Release:        1.el7
Summary:        Release packages for the Puppet repository
Group:          System Environment/Base
License:        ASL 2.0
URL:            https://packages.microsoft.com/rhel/7/prod/
Source0:        RPM-GPG-KEY-puppet-release
Source1:        bill-of-materials
Source2:        puppet.repo

BuildArch:     noarch
Requires:      redhat-release >=  7

%description
Release packages for the Puppet repository

Contains the following components:
gpg_key 2017.06.22
repo_definition 2017.06.22

%prep
%setup -q  -c -T
install -pm 644 %{SOURCE0} .
install -pm 644 %{SOURCE1} .

%build

%install
rm -rf $RPM_BUILD_ROOT
install -Dpm 644 %{SOURCE0} $RPM_BUILD_ROOT%{_sysconfdir}/pki/rpm-gpg/RPM-GPG-KEY-puppet-release
install -dm 755 $RPM_BUILD_ROOT%{_sysconfdir}/yum.repos.d
install -pm 644 %{SOURCE2} $RPM_BUILD_ROOT%{_sysconfdir}/yum.repos.d

%clean
rm -rf $RPM_BUILD_ROOT

%files
%defattr(-,root,root,-)
%doc bill-of-materials
%config(noreplace) /etc/yum.repos.d/*
/etc/pki/rpm-gpg/*

%changelog
* Sat Jul 14 2018 <grainger@gmail.com> -  1.0.0-1.el7
- Initial packaging
