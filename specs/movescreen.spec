%global git_proj liger1978
%global git_app movescreen
%global git_rel 0.0.1
%global git_rev eee8549898af745a572ed2c68cf9911ee9c940d7
%global git_rev_short eee8549
%global git_date 20180928

Name:            %{git_app}
Version:         %{git_rel}.%{git_date}git%{git_rev_short}
Release:         1.el7.harbottle
Summary:         Moves the window with focus on an adjacent monitor
License:         Public domain
URL:             https://github.com/%{git_proj}/%{git_app}
Source0:         %{url}/archive/%{git_rev}.tar.gz
Requires:        python
Requires:        xorg-x11-server-utils

%description
Moves the window with focus on an adjacent monitor. It is intendend for window managers lacking this shortcut like XFCE.

%prep
%setup -q -n %{git_app}-%{git_rev}

%install
install -d -m 755 $RPM_BUILD_ROOT%{_bindir}
install -m 755 %{name}.py $RPM_BUILD_ROOT%{_bindir}/%{name}

%files
%{_bindir}/%{name}

%changelog
* Fri Sep 28 2018 Richard Grainger <grainger@gmail.com> - 0.1.1.20180928giteee8549-1.el7.harbottle
- Initial package
