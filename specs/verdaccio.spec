%global modname verdaccio
%global node_version 13.7.0 

Name:            verdaccio
Version:         4.11.0
Release:         1%{?dist}.harbottle
Summary:         A lightweight private npm proxy registry
Group:           Applications/System
License:         MIT
Url:             https://%{modname}.org
%if 0%{?rhel} == 7
Source0:         https://github.com/%{modname}/%{modname}/archive/v%{version}.tar.gz
Source1:         shim.sh
Source2:         verdaccio.sh
BuildRequires:   nodejs
BuildRequires:   python-virtualenv
BuildRequires:   systemd-units
Requires(post):  systemd
Requires(preun): systemd
%endif
%if 0%{?rhel} == 8
Requires:        nodejs-verdaccio = %{version}
%endif

%description
Verdaccio is a lightweight private npm proxy registry built in Node.js.

%prep
%if 0%{?rhel} == 7
%setup -q -n %{modname}-%{version}
%endif

%build
%if 0%{?rhel} == 7
virtualenv temp
./temp/bin/pip install nodeenv
./temp/bin/nodeenv --node=%{node_version} --prebuilt %{buildroot}%{_datadir}/%{modname}
%{buildroot}%{_datadir}/%{modname}/bin/shim %{buildroot}%{_datadir}/%{modname}/bin/npm install -g --prefix=%{buildroot}%{_datadir}/%{modname} verdaccio@4.4.2
install -m 755 %{SOURCE1} %{buildroot}%{_datadir}/%{modname}/bin/shim
install -d -m 755 %{buildroot}%{_bindir}
install -d -m 755 %{buildroot}%{_unitdir}
install -d -m 755 %{buildroot}%{_sharedstatedir}/%{modname}
install -d -m 755 %{buildroot}%{_sysconfdir}
install -d -m 755 %{buildroot}%{_sysconfdir}/%{modname}
install -d -m 755 %{buildroot}%{_unitdir}
install -m 755 %{SOURCE2} %{buildroot}%{_bindir}/%{modname}
sed -ie 's_storage:.*_storage: /var/lib/verdaccio_g' %{buildroot}%{_datadir}/%{modname}/lib/node_modules/%{modname}/conf/default.yaml
cp %{buildroot}%{_datadir}/%{modname}/lib/node_modules/%{modname}/conf/default.yaml %{buildroot}%{_sysconfdir}/%{modname}/config.yaml
touch %{buildroot}%{_sysconfdir}/%{modname}/htpasswd
sed -ie 's_ExecStart=.*_ExecStart=/usr/share/verdaccio/bin/shim /usr/share/verdaccio/bin/verdaccio --config /etc/verdaccio/config.yaml_g' %{buildroot}%{_datadir}/%{modname}/lib/node_modules/%{modname}/systemd/%{modname}.service
cp %{buildroot}%{_datadir}/%{modname}/lib/node_modules/%{modname}/systemd/%{modname}.service %{buildroot}%{_unitdir}/%{modname}.service
%endif

%if 0%{?rhel} == 7
%pre
getent group %{modname} >/dev/null || groupadd -f -r %{modname}
getent passwd %{modname} >/dev/null || useradd -r -g %{modname} -d %{_sharedstatedir}/%{modname} -s /sbin/nologin -c "%{modname} user" %{modname}
exit 0

%post
%systemd_post %{modname}.service

%preun
%systemd_preun %{modname}.service

%postun
%systemd_postun_with_restart %{modname}.service
%endif

%files
%if 0%{?rhel} == 7
%license LICENSE
%doc *.md
%attr(-,%{modname},%{modname}) %{_sharedstatedir}/%{modname}
%config(noreplace) %{_sysconfdir}/%{modname}/config.yaml
%attr(0660,%{modname},%{modname}) %config(noreplace) %{_sysconfdir}/%{modname}/htpasswd
%{_datadir}/%{modname}
%{_bindir}/%{modname}
%{_unitdir}/%{modname}.service
%endif

%changelog
* Wed Jan 20 2021 - harbottle@room3d3.com - 4.11.0-1
  - Bump version

* Sun Dec 06 2020 - harbottle@room3d3.com - 4.10.0-1
  - Bump version

* Sun Nov 29 2020 - harbottle@room3d3.com - 4.9.1-1
  - Bump version

* Sun Nov 22 2020 - harbottle@room3d3.com - 4.9.0-1
  - Bump version

* Thu Aug 06 2020 - harbottle@room3d3.com - 4.8.1-1
  - Bump version

* Fri Jul 24 2020 - harbottle@room3d3.com - 4.8.0-1
  - Bump version

* Fri Jun 26 2020 - harbottle@room3d3.com - 4.7.2-1
  - Bump version

* Mon Jun 22 2020 - harbottle@room3d3.com - 4.7.1-1
  - Bump version

* Sat Jun 20 2020 - harbottle@room3d3.com - 4.7.0-1
  - Bump version

* Fri May 01 2020 - harbottle@room3d3.com - 4.6.2-1
  - Bump version

* Mon Apr 27 2020 - harbottle@room3d3.com - 4.6.1-1
  - Bump version

* Fri Apr 24 2020 - harbottle@room3d3.com - 4.6.0-1
  - Bump version

* Sat Mar 14 2020 - harbottle@room3d3.com - 4.5.1-1
  - Bump version

* Sat Mar 14 2020 - harbottle@room3d3.com - 4.5.0-1
  - Bump version

* Sat Feb 29 2020 - harbottle@room3d3.com - 4.4.4-1
  - Bump version

* Sat Jan 25 2020 - harbottle@room3d3.com - 4.4.2-3
  - Fix storage

* Sat Jan 25 2020 - harbottle@room3d3.com - 4.4.2-2
  - Build for el7

* Sat Jan 11 2020 - harbottle@room3d3.com - 4.4.2-1
  - Bump version

* Sat Jan 04 2020 - harbottle@room3d3.com - 4.4.1-2
  - Tidy spec file

* Fri Jan 03 2020 - harbottle@room3d3.com - 4.4.1-1
  - Bump version

* Fri Jan 03 2020 - harbottle@room3d3.com - 4.4.0-1
  - Initial package
