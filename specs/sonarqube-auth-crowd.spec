%define __jar_repack 0

Name:     sonarqube-auth-crowd
Version:  2.1.3
Release:  2%{?dist}.harbottle
Summary:  SonarQube Crowd authentication plugin
Group:    Applications/System
License:  LGPL-3.0
URL:      https://github.com/deepy/sonar-crowd
Source0:  %{url}/releases/download/%{version}/sonar-crowd-plugin-%{version}.jar
Autoprov: no

%description
Crowd authentication plugin for SonarQube.

%install
install -d -m 755 $RPM_BUILD_ROOT%{_var}/lib/sonarqube/extensions/plugins
mv %{SOURCE0} $RPM_BUILD_ROOT%{_var}/lib/sonarqube/extensions/plugins/sonar-crowd-plugin-%{version}.jar

%pre
rm -f %{_var}/lib/sonarqube/extensions/plugins/sonar-crowd-plugin-*.jar
getent group sonarqube >/dev/null || groupadd -f -r sonarqube
getent passwd sonarqube >/dev/null || useradd -r -g sonarqube -d /usr/share/sonarqube -s /sbin/nologin -c "SonarQube user" sonarqube
exit 0

%files
%attr(0644,sonarqube,sonarqube) %{_var}/lib/sonarqube/extensions/plugins/sonar-crowd-plugin-%{version}.jar

%changelog
* Tue Dec 10 2019 - harbottle@room3d3.com - 2.1.3-2
  - Spec file changes for el8

* Fri Dec 06 2019 - harbottle@room3d3.com - 2.1.3-1
  - Bump version

* Fri Dec 06 2019 - harbottle@room3d3.com - 2.1.2-1
  - Initial packaging
