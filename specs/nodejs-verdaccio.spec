%global modname verdaccio

Name:            nodejs-verdaccio
Version:         4.11.0
Release:         1%{?dist}.harbottle
Summary:         A lightweight private npm proxy registry
Group:           Applications/System
License:         MIT
Url:             https://%{modname}.org
Source0:         https://github.com/%{modname}/%{modname}/archive/v%{version}.tar.gz
BuildRequires:   nodejs >= 8
BuildRequires:   systemd-units
Requires:        nodejs >= 8
Requires(post):  systemd
Requires(preun): systemd

%description
Verdaccio is a lightweight private npm proxy registry built in Node.js.

%prep
%setup -q -n %{modname}-%{version}

%build
npm install -g --prefix=%{buildroot}/usr %{modname}@%{version}
install -d -m 755 %{buildroot}%{_sharedstatedir}/%{modname}
install -d -m 755 %{buildroot}%{_sysconfdir}
install -d -m 755 %{buildroot}%{_sysconfdir}/%{modname}
install -d -m 755 %{buildroot}%{_unitdir}
sed -ie 's_storage:.*_storage: /var/lib/verdaccio_g' %{buildroot}/usr/lib/node_modules/%{modname}/conf/default.yaml
cp %{buildroot}/usr/lib/node_modules/%{modname}/conf/default.yaml %{buildroot}%{_sysconfdir}/%{modname}/config.yaml
touch %{buildroot}%{_sysconfdir}/%{modname}/htpasswd
cp %{buildroot}/usr/lib/node_modules/%{modname}/systemd/%{modname}.service %{buildroot}%{_unitdir}/%{modname}.service

%pre
getent group %{modname} >/dev/null || groupadd -f -r %{modname}
getent passwd %{modname} >/dev/null || useradd -r -g %{modname} -d %{_sharedstatedir}/%{modname} -s /sbin/nologin -c "%{modname} user" %{modname}
exit 0

%post
%systemd_post %{modname}.service

%preun
%systemd_preun %{modname}.service

%postun
%systemd_postun_with_restart %{modname}.service

%files
%license LICENSE
%doc *.md
%attr(-,%{modname},%{modname}) %{_sharedstatedir}/%{modname}
%config(noreplace) %{_sysconfdir}/%{modname}/config.yaml
%attr(0660,%{modname},%{modname}) %config(noreplace) %{_sysconfdir}/%{modname}/htpasswd
/usr/lib/node_modules/%{modname}
%{_bindir}/%{modname}
%{_unitdir}/%{modname}.service

%changelog
* Wed Jan 20 2021 - harbottle@room3d3.com - 4.11.0-1
  - Bump version

* Sun Dec 06 2020 - harbottle@room3d3.com - 4.10.0-1
  - Bump version

* Sun Nov 29 2020 - harbottle@room3d3.com - 4.9.1-1
  - Bump version

* Sun Nov 22 2020 - harbottle@room3d3.com - 4.9.0-1
  - Bump version

* Thu Aug 06 2020 - harbottle@room3d3.com - 4.8.1-1
  - Bump version

* Fri Jul 24 2020 - harbottle@room3d3.com - 4.8.0-1
  - Bump version

* Fri Jun 26 2020 - harbottle@room3d3.com - 4.7.2-1
  - Bump version

* Mon Jun 22 2020 - harbottle@room3d3.com - 4.7.1-1
  - Bump version

* Sat Jun 20 2020 - harbottle@room3d3.com - 4.7.0-1
  - Bump version

* Fri May 01 2020 - harbottle@room3d3.com - 4.6.2-1
  - Bump version

* Mon Apr 27 2020 - harbottle@room3d3.com - 4.6.1-1
  - Bump version

* Fri Apr 24 2020 - harbottle@room3d3.com - 4.6.0-1
  - Bump version

* Sat Mar 14 2020 - harbottle@room3d3.com - 4.5.1-1
  - Bump version

* Sat Mar 14 2020 - harbottle@room3d3.com - 4.5.0-1
  - Bump version

* Sat Feb 29 2020 - harbottle@room3d3.com - 4.4.4-1
  - Bump version

* Sat Jan 25 2020 - harbottle@room3d3.com - 4.4.2-3
  - Fix storage

* Sat Jan 25 2020 - harbottle@room3d3.com - 4.4.2-2
  - Fix dependencies

* Sat Jan 11 2020 - harbottle@room3d3.com - 4.4.2-1
  - Bump version

* Sat Jan 04 2020 - harbottle@room3d3.com - 4.4.1-2
  - Tidy spec file

* Fri Jan 03 2020 - harbottle@room3d3.com - 4.4.1-1
  - Bump version

* Fri Jan 03 2020 - harbottle@room3d3.com - 4.4.0-1
  - Initial package
