%define __jar_repack 0

Name:     sonarqube-javascript
Version:  7.2.0.14938
Release:  1%{?dist}.harbottle
Summary:  SonarQube JavaScript plugin
Group:    Applications/System
License:  LGPL-3.0
Source0:  https://binaries.sonarsource.com/Distribution/sonar-javascript-plugin/sonar-javascript-plugin-%{version}.jar
Autoprov: no
Obsoletes: sonarqube-js

%description
JavaScript plugin for SonarQube.

%prep

%install
install -d -m 755 $RPM_BUILD_ROOT%{_var}/lib/sonarqube/extensions/plugins
mv %{SOURCE0} $RPM_BUILD_ROOT%{_var}/lib/sonarqube/extensions/plugins

%pre
rm -f %{_var}/lib/sonarqube/extensions/plugins/sonar-javascript-plugin-*.jar
getent group sonarqube >/dev/null || groupadd -f -r sonarqube
getent passwd sonarqube >/dev/null || useradd -r -g sonarqube -d /usr/share/sonarqube -s /sbin/nologin -c "SonarQube user" sonarqube
exit 0

%files
%attr(0644,sonarqube,sonarqube) %{_var}/lib/sonarqube/extensions/plugins/sonar-javascript-plugin-%{version}.jar

%changelog
* Fri Feb 12 2021 - harbottle@room3d3.com - 7.2.0.14938-1
  - Bump version

* Tue Dec 22 2020 - harbottle@room3d3.com - 7.1.0.14721-1
  - Bump version

* Tue Dec 08 2020 - harbottle@room3d3.com - 7.0.1.14561-1
  - Bump version

* Fri Dec 04 2020 - harbottle@room3d3.com - 7.0.0.14528-1
  - Bump version

* Thu Nov 12 2020 - harbottle@room3d3.com - 6.7.0.14237-1
  - Bump version

* Thu Oct 22 2020 - harbottle@room3d3.com - 6.6.0.13923-1
  - Bump version

* Mon Sep 28 2020 - harbottle@room3d3.com - 6.5.0.13383-1
  - Bump version

* Wed Aug 26 2020 - harbottle@room3d3.com - 6.4.1.12828-1
  - Bump version

* Mon Aug 24 2020 - harbottle@room3d3.com - 6.4.0.12803-1
  - Bump version

* Fri Jul 24 2020 - harbottle@room3d3.com - 6.3.0.12464-1
  - Bump version

* Fri Apr 03 2020 - harbottle@room3d3.com - 6.2.1.12157-1
  - Bump version

* Tue Jan 21 2020 - harbottle@room3d3.com - 6.2.0.12043-1
  - Bump version

* Tue Dec 10 2019 - harbottle@room3d3.com - 6.1.0.11503-2
  - Spec file changes for el8

* Thu Nov 14 2019 - harbottle@room3d3.com - 6.1.0.11503-1
  - Bump version

* Tue Oct 15 2019 - harbottle@room3d3.com - 6.0.1.10206-1
  - Bump version

* Wed Oct 02 2019 - harbottle@room3d3.com - 6.0.0.9595-1
  - Bump version

* Sun Jul 21 2019 - harbottle@room3d3.com - 5.2.1.7778-3
  - Fix plugin ownership

* Sun Jul 21 2019 - harbottle@room3d3.com - 5.2.1.7778-2
  - Standardize SonarQube plugins

* Thu Jun 13 2019 - harbottle@room3d3.com - 5.2.1.7778-1
  - Bump version

* Tue Jun 11 2019 - harbottle@room3d3.com - 5.2.0.7766-1
  - Bump version

* Mon Mar 11 2019 - harbottle@room3d3.com - 5.1.1.7506-1
  - Bump version

* Mon Feb 25 2019 - harbottle@room3d3.com - 5.1.0.7456-1
  - Bump version

* Mon Oct 15 2018 grainger@gmail.com - 5.0.0.6962-1
  - Bump version

* Mon Aug 06 2018 grainger@gmail.com - 4.2.0.6476-1
  - Bump version

* Sat Mar 03 2018 grainger@gmail.com - 4.1.0.6085-1
  - Bump version

* Thu Dec 21 2017 grainger@gmail.com - 4.0.0.5862-1
  - Bump version

* Fri Nov 17 2017 grainger@gmail.com - 3.3.0.5702-2
  - Bump iteration due to build complications

* Tue Nov 14 2017 grainger@gmail.com - 3.3.0.5702-1
  - Bump version
* Mon Oct 23 2017 grainger@gmail.com - 3.2.0.5506-1
  - Initial packaging
