%global git_owner rightscale
%global git_repo %{name}
%global git_rev 1c92a380d04c351b203be89ba1f4264c2db1b4fc
%global git_date 20181005
%global git_archive_file %{git_rev}.zip
%global git_archive_dir %{git_repo}-%{git_rev}
%global go_namespace github.com/%{git_owner}/%{git_repo}

Name:          wstunnel
Version:       1.0.6.20181005git1c92a38
Release:       1.el7.harbottle
Summary:       Web-sockets tunnel for HTTP requests
Group:         Applications/System
License:       MIT
Url:           https://github.com/%{git_owner}/%{git_repo}
Source0:       %{url}/archive/%{git_archive_file}
Source1:       wstuncli.sysconfig
Source2:       wstunsrv.sysconfig
Source3:       wstuncli.service
Source4:       wstunsrv.service
BuildRequires: golang go-dep systemd-units
Requires(post):  systemd
Requires(preun): systemd

%description
WStunnel creates an HTTPS tunnel that can connect servers sitting behind an HTTP
proxy and firewall to clients on the internet. It differs from many other
projects by handling many concurrent tunnels allowing a central client (or set
of clients) to make requests to many servers sitting behind firewalls. Each
client/server pair are joined through a rendez-vous token

%prep
%setup -q -n %{git_archive_dir}

%build
%define debug_package %{nil}
export GOPATH=$PWD
mkdir -p src/%{go_namespace}/
shopt -s extglob dotglob
mv !(src) src/%{go_namespace}/
shopt -u extglob dotglob
pushd src/%{go_namespace}/
dep ensure
echo -e "package main\n\nconst VV = \"%{version}\"" > version.go
go build
popd

%install
install -d -m 755 $RPM_BUILD_ROOT%{_bindir}
install -d -m 755 $RPM_BUILD_ROOT%{_sysconfdir}/sysconfig
install -d -m 755 $RPM_BUILD_ROOT%{_unitdir}
install -d -m 755 $RPM_BUILD_ROOT%{_var}/empty/%{name}
install -m 755 src/%{go_namespace}/%{name} $RPM_BUILD_ROOT%{_bindir}
install -m 644 %{SOURCE1} $RPM_BUILD_ROOT%{_sysconfdir}/sysconfig/wstuncli
install -m 644 %{SOURCE2} $RPM_BUILD_ROOT%{_sysconfdir}/sysconfig/wstunsrv
install -m 644 %{SOURCE3} $RPM_BUILD_ROOT%{_unitdir}
install -m 644 %{SOURCE4} $RPM_BUILD_ROOT%{_unitdir}

%pre
getent group %{name} >/dev/null || groupadd -f -r %{name}
getent passwd %{name} >/dev/null || useradd -r -g %{name} -d /var/empty/%{name} -s /sbin/nologin -c "%{name} user" %{name}
exit 0

%systemd_post wstuncli.service
%systemd_post wstunsrv.service

%preun
%systemd_preun wstuncli.service
%systemd_preun wstunsrv.service

%postun
%systemd_postun_with_restart wstuncli.service
%systemd_postun_with_restart wstunsrv.service

%files
%license src/%{go_namespace}/LICENSE
%doc src/%{go_namespace}/README.md
%{_bindir}/%{name}
%config(noreplace) %{_sysconfdir}/sysconfig/wstuncli
%config(noreplace) %{_sysconfdir}/sysconfig/wstunsrv
%{_unitdir}/wstuncli.service
%{_unitdir}/wstunsrv.service
%attr(-,%{name},%{name}) %{_var}/empty/%{name}

%changelog
* Tue Jul 16 2019 - harbottle@room3d3.com - 1.0.6.20181005git1c92a38
  - Initial package
