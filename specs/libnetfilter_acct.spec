Name:           libnetfilter_acct
Version:        1.0.3
Release:        1.el7.harbottle
Summary:        A library providing interface to extended accounting infrastructure

Group:          System Environment/Libraries
License:        LGPLv2+
URL:            http://www.netfilter.org/projects/libnetfilter_acct/index.html
Source0:        http://www.netfilter.org/projects/libnetfilter_acct/files/libnetfilter_acct-%{version}.tar.bz2

BuildRequires:  libmnl-devel

%description
libnetfilter_acct is the userspace library providing interface to extended
accounting infrastructure.

libnetfilter_acct is used by nfacct.

%package        devel
Summary:        Development files for %{name}
Requires:       %{name}%{?_isa} = %{version}-%{release}

%description    devel
The %{name}-devel package contains libraries and header files for
developing applications that use %{name}.


%prep
%setup -q


%build
%configure --disable-static
make %{?_smp_mflags}


%install
make install DESTDIR=$RPM_BUILD_ROOT
find $RPM_BUILD_ROOT -name '*.la' -exec rm -f {} ';'
find examples '(' -name Makefile.in -o -name Makefile.am ')' -exec rm -f {} ';'
mv examples examples-%{_arch}

%post -p /sbin/ldconfig

%postun -p /sbin/ldconfig


%files
%doc COPYING README
%{_libdir}/*.so.*

%files devel
%doc COPYING
%doc examples-%{_arch}
%{_includedir}/*
%{_libdir}/*.so
%{_libdir}/pkgconfig/%{name}.pc


%changelog
* Mon Dec 31 2018 - harbottle@room3d3.com - 1.0.3-1
  - Bump version

* Wed Apr 18 2018 Richard Grainger <grainger@gmail.com> - 1.0.2-9
- Adapted for harbottle-main EL7 repo

* Thu Aug 03 2017 Fedora Release Engineering <releng@fedoraproject.org> - 1.0.2-8
- Rebuilt for https://fedoraproject.org/wiki/Fedora_27_Binutils_Mass_Rebuild

* Wed Jul 26 2017 Fedora Release Engineering <releng@fedoraproject.org> - 1.0.2-7
- Rebuilt for https://fedoraproject.org/wiki/Fedora_27_Mass_Rebuild

* Fri Feb 10 2017 Fedora Release Engineering <releng@fedoraproject.org> - 1.0.2-6
- Rebuilt for https://fedoraproject.org/wiki/Fedora_26_Mass_Rebuild

* Thu Feb 04 2016 Fedora Release Engineering <releng@fedoraproject.org> - 1.0.2-5
- Rebuilt for https://fedoraproject.org/wiki/Fedora_24_Mass_Rebuild

* Wed Jun 17 2015 Fedora Release Engineering <rel-eng@lists.fedoraproject.org> - 1.0.2-4
- Rebuilt for https://fedoraproject.org/wiki/Fedora_23_Mass_Rebuild

* Sun Aug 17 2014 Fedora Release Engineering <rel-eng@lists.fedoraproject.org> - 1.0.2-3
- Rebuilt for https://fedoraproject.org/wiki/Fedora_21_22_Mass_Rebuild

* Sat Jun 07 2014 Fedora Release Engineering <rel-eng@lists.fedoraproject.org> - 1.0.2-2
- Rebuilt for https://fedoraproject.org/wiki/Fedora_21_Mass_Rebuild

* Fri Sep 13 2013 Hushan Jia <hushan.jia@gmail.com> - 1.0.2-1
- Update to upstream 1.0.2

* Sat Aug 03 2013 Fedora Release Engineering <rel-eng@lists.fedoraproject.org> - 1.0.0-4
- Rebuilt for https://fedoraproject.org/wiki/Fedora_20_Mass_Rebuild

* Thu Feb 14 2013 Fedora Release Engineering <rel-eng@lists.fedoraproject.org> - 1.0.0-3
- Rebuilt for https://fedoraproject.org/wiki/Fedora_19_Mass_Rebuild

* Wed Nov 28 2012 Hushan Jia <hushan.jia@gmail.com> - 1.0.0-2
- Fix Requires and BuildRequires Tag
- Fix el5 build requirement

* Thu Aug 16 2012 Hushan Jia <hushan.jia@gmail.com> - 1.0.0-1
- initial packaging

