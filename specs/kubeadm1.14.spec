%global owner kubernetes
%global repo kubernetes
%global host github.com
%global archive v%{version}.tar.gz
%global dir %{repo}-%{version}
%global namespace k8s.io/%{repo}
%global cmd kubeadm
%global tree 1.14
%global go_version 1.12.5

Name:          %{cmd}%{tree}
Version:       1.14.10
Release:       1%{?dist}.harbottle
Summary:       Kubernetes tool for standing up clusters
Group:         Applications/System
License:       Apache 2.0
Url:           https://%{host}/%{owner}/%{repo}
Source0:       %{url}/archive/%{archive}
Source1:       https://dl.google.com/go/go%{go_version}.linux-amd64.tar.gz
BuildRequires: which make rsync

%description
Kubernetes tool for standing up clusters. This package provides an
up-to-date version of the command. It can be run using command: %{name}

%prep
%setup -q -n %{dir}
%setup -q -T -D -a 1 -n %{dir}

%build
%define debug_package %{nil}
export GOPATH=$PWD
export GOROOT=$PWD/go
export PATH=$GOPATH/bin:$GOROOT/bin:$PATH
mkdir -p src/%{namespace}/
shopt -s extglob dotglob
mv !(src|go) src/%{namespace}/
shopt -u extglob dotglob
pushd src/%{namespace}/
make %{cmd}
popd

%install
install -d -m 755 $RPM_BUILD_ROOT%{_bindir}
install -m 755 src/%{namespace}/_output/bin/%{cmd} $RPM_BUILD_ROOT%{_bindir}/%{name}

%files
%license src/%{namespace}/LICENSE
%doc src/%{namespace}/{*.md,OWNERS,SECURITY_CONTACTS}
%{_bindir}/%{name}

%changelog
* Thu Dec 19 2019 - harbottle@room3d3.com - 1.14.10-1
  - Bump version

* Wed Nov 13 2019 - harbottle@room3d3.com - 1.14.9-1
  - Bump version

* Tue Oct 15 2019 - harbottle@room3d3.com - 1.14.8-1
  - Bump version

* Wed Sep 18 2019 - harbottle@room3d3.com - 1.14.7-1
  - Bump version

* Mon Aug 19 2019 - harbottle@room3d3.com - 1.14.6-1
  - Bump version

* Mon Aug 05 2019 - harbottle@room3d3.com - 1.14.5-1
  - Bump version

* Mon Jul 08 2019 - harbottle@room3d3.com - 1.14.4-1
  - Bump version

* Fri Jun 07 2019 - harbottle@room3d3.com - 1.14.3-1
  - Initial package

