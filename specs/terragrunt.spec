Name:    terragrunt
Version: 0.28.7
Release: 1%{?dist}.harbottle
Summary: Terragrunt is a thin wrapper for Terraform
Group:   Applications/System
License: MIT
URL:     https://terragrunt.gruntwork.io/
Source0: https://github.com/gruntwork-io/%{name}/archive/v%{version}.tar.gz
Source1: https://github.com/gruntwork-io/%{name}/releases/download/v%{version}/%{name}_linux_amd64

%description
Terragrunt is a thin wrapper for Terraform that provides extra tools for keeping
your Terraform configurations DRY, working with multiple Terraform modules, and
managing remote state.

%prep
%setup -q

%install
install -d -m 755 $RPM_BUILD_ROOT%{_bindir}
install -m 0755 %{SOURCE1} $RPM_BUILD_ROOT%{_bindir}/%{name}

%files
%license LICENSE.txt
%doc README.md
%{_bindir}/%{name}

%changelog
* Thu Feb 18 2021 - harbottle@room3d3.com - 0.28.7-1
  - Bump version

* Wed Feb 17 2021 - harbottle@room3d3.com - 0.28.6-1
  - Bump version

* Tue Feb 16 2021 - harbottle@room3d3.com - 0.28.5-1
  - Bump version

* Fri Feb 12 2021 - harbottle@room3d3.com - 0.28.4-1
  - Bump version

* Wed Feb 10 2021 - harbottle@room3d3.com - 0.28.3-1
  - Bump version

* Thu Feb 04 2021 - harbottle@room3d3.com - 0.28.2-1
  - Bump version

* Tue Feb 02 2021 - harbottle@room3d3.com - 0.28.1-1
  - Bump version

* Tue Feb 02 2021 - harbottle@room3d3.com - 0.28.0-1
  - Bump version

* Mon Feb 01 2021 - harbottle@room3d3.com - 0.27.4-1
  - Bump version

* Tue Jan 26 2021 - harbottle@room3d3.com - 0.27.3-1
  - Bump version

* Mon Jan 25 2021 - harbottle@room3d3.com - 0.27.2-1
  - Bump version

* Tue Jan 12 2021 - harbottle@room3d3.com - 0.27.1-1
  - Bump version

* Fri Jan 08 2021 - harbottle@room3d3.com - 0.27.0-1
  - Bump version

* Wed Nov 25 2020 - harbottle@room3d3.com - 0.26.7-1
  - Bump version

* Wed Nov 25 2020 - harbottle@room3d3.com - 0.26.6-1
  - Bump version

* Wed Nov 25 2020 - harbottle@room3d3.com - 0.26.5-1
  - Bump version

* Fri Nov 20 2020 - harbottle@room3d3.com - 0.26.4-1
  - Bump version

* Fri Nov 06 2020 - harbottle@room3d3.com - 0.26.2-1
  - Bump version

* Mon Nov 02 2020 - harbottle@room3d3.com - 0.26.0-1
  - Bump version

* Fri Oct 23 2020 - harbottle@room3d3.com - 0.25.5-1
  - Bump version

* Fri Oct 16 2020 - harbottle@room3d3.com - 0.25.4-1
  - Bump version

* Fri Oct 09 2020 - harbottle@room3d3.com - 0.25.3-1
  - Bump version

* Wed Sep 30 2020 - harbottle@room3d3.com - 0.25.2-1
  - Bump version

* Sun Sep 20 2020 - harbottle@room3d3.com - 0.25.1-1
  - Bump version

* Sat Sep 19 2020 - harbottle@room3d3.com - 0.25.0-1
  - Bump version

* Wed Sep 16 2020 - harbottle@room3d3.com - 0.24.4-1
  - Bump version

* Tue Sep 15 2020 - harbottle@room3d3.com - 0.24.3-1
  - Bump version

* Thu Sep 10 2020 - harbottle@room3d3.com - 0.24.1-1
  - Bump version

* Tue Sep 08 2020 - harbottle@room3d3.com - 0.24.0-1
  - Bump version

* Wed Sep 02 2020 - harbottle@room3d3.com - 0.23.40-1
  - Bump version

* Fri Aug 28 2020 - harbottle@room3d3.com - 0.23.38-1
  - Bump version

* Thu Aug 27 2020 - harbottle@room3d3.com - 0.23.37-1
  - Bump version

* Wed Aug 26 2020 - harbottle@room3d3.com - 0.23.36-1
  - Bump version

* Tue Aug 25 2020 - harbottle@room3d3.com - 0.23.35-1
  - Bump version

* Mon Aug 24 2020 - harbottle@room3d3.com - 0.23.34-1
  - Bump version

* Mon Aug 03 2020 - harbottle@room3d3.com - 0.23.33-1
  - Bump version

* Fri Jul 31 2020 - harbottle@room3d3.com - 0.23.32-1
  - Bump version

* Wed Jul 01 2020 - harbottle@room3d3.com - 0.23.31-1
  - Bump version

* Mon Jun 29 2020 - harbottle@room3d3.com - 0.23.30-1
  - Bump version

* Thu Jun 25 2020 - harbottle@room3d3.com - 0.23.29-1
  - Bump version

* Tue Jun 23 2020 - harbottle@room3d3.com - 0.23.28-1
  - Bump version

* Sun Jun 14 2020 - harbottle@room3d3.com - 0.23.27-1
  - Bump version

* Fri Jun 12 2020 - harbottle@room3d3.com - 0.23.26-1
  - Bump version

* Thu Jun 11 2020 - harbottle@room3d3.com - 0.23.25-1
  - Bump version

* Mon Jun 08 2020 - harbottle@room3d3.com - 0.23.24-1
  - Bump version

* Fri May 29 2020 - harbottle@room3d3.com - 0.23.23-1
  - Bump version

* Thu May 28 2020 - harbottle@room3d3.com - 0.23.22-1
  - Bump version

* Thu May 28 2020 - harbottle@room3d3.com - 0.23.21-1
  - Bump version

* Sat May 23 2020 - harbottle@room3d3.com - 0.23.20-1
  - Bump version

* Fri May 15 2020 - harbottle@room3d3.com - 0.23.18-1
  - Bump version

* Tue May 12 2020 - harbottle@room3d3.com - 0.23.17-1
  - Bump version

* Mon May 11 2020 - harbottle@room3d3.com - 0.23.16-1
  - Bump version

* Fri May 08 2020 - harbottle@room3d3.com - 0.23.15-1
  - Bump version

* Tue May 05 2020 - harbottle@room3d3.com - 0.23.14-1
  - Bump version

* Thu Apr 30 2020 - harbottle@room3d3.com - 0.23.13-1
  - Bump version

* Mon Apr 27 2020 - harbottle@room3d3.com - 0.23.12-1
  - Bump version

* Mon Apr 27 2020 - harbottle@room3d3.com - 0.23.11-1
  - Bump version

* Thu Apr 16 2020 - harbottle@room3d3.com - 0.23.10-1
  - Bump version

* Wed Apr 15 2020 - harbottle@room3d3.com - 0.23.9-1
  - Bump version

* Wed Apr 08 2020 - harbottle@room3d3.com - 0.23.8-1
  - Bump version

* Wed Apr 08 2020 - harbottle@room3d3.com - 0.23.7-1
  - Bump version

* Mon Apr 06 2020 - harbottle@room3d3.com - 0.23.6-1
  - Bump version

* Sat Apr 04 2020 - harbottle@room3d3.com - 0.23.5-1
  - Bump version

* Sun Mar 29 2020 - harbottle@room3d3.com - 0.23.4-1
  - Bump version

* Sat Mar 28 2020 - harbottle@room3d3.com - 0.23.3-1
  - Bump version

* Tue Mar 10 2020 - harbottle@room3d3.com - 0.23.2-1
  - Bump version

* Mon Mar 09 2020 - harbottle@room3d3.com - 0.23.1-1
  - Bump version

* Tue Mar 03 2020 - harbottle@room3d3.com - 0.23.0-1
  - Bump version

* Sat Feb 29 2020 - harbottle@room3d3.com - 0.22.5-1
  - Bump version

* Wed Feb 12 2020 - harbottle@room3d3.com - 0.21.13-1
  - Bump version

* Mon Feb 10 2020 - harbottle@room3d3.com - 0.21.12-1
  - Bump version

* Sat Jan 18 2020 - harbottle@room3d3.com - 0.21.11-1
  - Initial package
