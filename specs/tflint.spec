%global namespace github.com/terraform-linters/%{name}

Name:          tflint
Version:       0.24.1
Release:       1%{?dist}.harbottle
Summary:       TFLint is a Terraform linter for detecting errors that can not be detected by terraform plan
Group:         Applications/System
License:       MPL-2.0
Url:           https://%{namespace}
Source0:       %{url}/archive/v%{version}.tar.gz
BuildRequires: golang make

%description
TFLint is a Terraform linter for detecting errors that can not be detected by
terraform plan.

Terraform is a great tool for infrastructure as a code. It generates an
execution plan, we can rely on this plan to proceed with development. However,
this plan does not verify values used in template.

%prep
%setup -q -n %{name}-%{version}

%build
%define debug_package %{nil}
export GOPROXY=https://proxy.golang.org
export GOPATH=$PWD
export PATH=$PATH:$GOPATH/bin
mkdir -p src/%{namespace}
shopt -s extglob dotglob
mv !(src) src/%{namespace}
shopt -u extglob dotglob
pushd src/%{namespace}
export GO111MODULE=on
make build
popd

%install
install -d -m 755 $RPM_BUILD_ROOT%{_bindir}
install -m 755 src/%{namespace}/dist/%{name} $RPM_BUILD_ROOT%{_bindir}

%files
%license src/%{namespace}/LICENSE
%doc src/%{namespace}/*.md
%doc src/%{namespace}/docs
%{_bindir}/%{name}

%changelog
* Mon Feb 01 2021 - harbottle@room3d3.com - 0.24.1-1
  - Bump version

* Sun Jan 31 2021 - harbottle@room3d3.com - 0.24.0-1
  - Bump version

* Sun Jan 10 2021 - harbottle@room3d3.com - 0.23.1-1
  - Bump version

* Sun Jan 03 2021 - harbottle@room3d3.com - 0.23.0-1
  - Bump version

* Thu Dec 10 2020 - harbottle@room3d3.com - 0.22.0-1
  - Bump version

* Mon Nov 23 2020 - harbottle@room3d3.com - 0.21.0-1
  - Bump version

* Sun Oct 18 2020 - harbottle@room3d3.com - 0.20.3-1
  - Bump version

* Wed Sep 23 2020 - harbottle@room3d3.com - 0.20.2-1
  - Bump version

* Sun Sep 13 2020 - harbottle@room3d3.com - 0.20.1-1
  - Bump version

* Sun Aug 23 2020 - harbottle@room3d3.com - 0.19.1-1
  - Bump version

* Thu Aug 20 2020 - harbottle@room3d3.com - 0.19.0-1
  - Bump version

* Fri Jul 24 2020 - harbottle@room3d3.com - 0.18.0-1
  - Bump version

* Sat Jun 27 2020 - harbottle@room3d3.com - 0.17.0-1
  - Bump version

* Sat Jun 06 2020 - harbottle@room3d3.com - 0.16.2-1
  - Bump version

* Thu May 21 2020 - harbottle@room3d3.com - 0.16.1-1
  - Bump version

* Sat May 16 2020 - harbottle@room3d3.com - 0.16.0-1
  - Bump version

* Sat Apr 25 2020 - harbottle@room3d3.com - 0.15.5-1
  - Bump version

* Sat Apr 04 2020 - harbottle@room3d3.com - 0.15.4-1
  - Bump version

* Sat Mar 21 2020 - harbottle@room3d3.com - 0.15.3-1
  - Bump version

* Sat Mar 14 2020 - harbottle@room3d3.com - 0.15.2-1
  - Bump version

* Sun Mar 01 2020 - harbottle@room3d3.com - 0.15.1-1
  - Bump version

* Sat Feb 29 2020 - harbottle@room3d3.com - 0.15.0-1
  - Bump version

* Thu Jan 30 2020 - harbottle@room3d3.com - 0.14.0-1
  - Bump version

* Fri Dec 27 2019 - harbottle@room3d3.com - 0.13.4-1
  - Bump version

* Thu Dec 19 2019 - harbottle@room3d3.com - 0.13.3-1
  - Bump version

* Mon Dec 09 2019 - harbottle@room3d3.com - 0.13.2-2
  - Update GitHub source repo
  - Update license
  - Add application group
  - Tidy-up spec file

* Sat Dec 07 2019 - harbottle@room3d3.com - 0.13.2-1
  - Fix build
  - Bump version

* Sat Nov 16 2019 - harbottle@room3d3.com - 0.13.1-1
  - Bump version

* Sat Oct 12 2019 - harbottle@room3d3.com - 0.12.1-1
  - Bump version

* Sat Sep 28 2019 - harbottle@room3d3.com - 0.12.0-1
  - Bump version

* Wed Sep 18 2019 - harbottle@room3d3.com - 0.11.2-1
  - Bump version

* Tue Sep 10 2019 - harbottle@room3d3.com - 0.11.1-1
  - Bump version

* Sun Sep 08 2019 - harbottle@room3d3.com - 0.11.0-1
  - Bump version

* Sat Aug 24 2019 - harbottle@room3d3.com - 0.10.3-1
  - Bump version

* Wed Aug 21 2019 - harbottle@room3d3.com - 0.10.1-1
  - Bump version

* Sat Aug 17 2019 - harbottle@room3d3.com - 0.10.0-1
  - Bump version

* Fri Aug 02 2019 - harbottle@room3d3.com - 0.9.3-1
  - Bump version

* Sat Jul 20 2019 - harbottle@room3d3.com - 0.9.2-1
  - Bump version

* Mon Jul 08 2019 - harbottle@room3d3.com - 0.9.1-1
  - Bump version

* Fri Jun 28 2019 - harbottle@room3d3.com - 0.9.0-1
  - Bump version

* Sun Jun 09 2019 - harbottle@room3d3.com - 0.8.3-1
  - Bump version

* Tue Jun 04 2019 - harbottle@room3d3.com - 0.8.2-1
  - Fix build
  - Bump version

* Wed Apr 03 2019 - harbottle@room3d3.com - 0.7.5-1
  - Bump version

* Sat Feb 09 2019 - harbottle@room3d3.com - 0.7.4-1
  - Bump version

* Mon Jan 07 2019 - harbottle@room3d3.com - 0.7.3-1
  - Initial package
