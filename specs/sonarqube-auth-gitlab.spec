%define __jar_repack 0

Name:     sonarqube-auth-gitlab
Version:  1.3.2
Release:  2%{?dist}.harbottle
Summary:  SonarQube GitLab authentication plugin
Group:    Applications/System
License:  LGPL-3.0
URL:      https://github.com/gabrie-allaigre/sonar-auth-gitlab-plugin
Source0:  %{url}/releases/download/%{version}/sonar-auth-gitlab-plugin-%{version}.jar
Autoprov: no

%description
GitLab authentication plugin for SonarQube.

%install
install -d -m 755 $RPM_BUILD_ROOT%{_var}/lib/sonarqube/extensions/plugins
mv %{SOURCE0} $RPM_BUILD_ROOT%{_var}/lib/sonarqube/extensions/plugins/sonar-auth-gitlab-plugin-%{version}.jar

%pre
rm -f %{_var}/lib/sonarqube/extensions/plugins/sonar-auth-gitlab-plugin-*.jar
getent group sonarqube >/dev/null || groupadd -f -r sonarqube
getent passwd sonarqube >/dev/null || useradd -r -g sonarqube -d /usr/share/sonarqube -s /sbin/nologin -c "SonarQube user" sonarqube
exit 0

%files
%attr(0644,sonarqube,sonarqube) %{_var}/lib/sonarqube/extensions/plugins/sonar-auth-gitlab-plugin-%{version}.jar

%changelog
* Tue Dec 10 2019 - harbottle@room3d3.com - 1.3.2-2
  - Spec file changes for el8

* Fri Dec 06 2019 - harbottle@room3d3.com - 1.3.2-1
  - Initial packaging
