
Name:    hadolint
Version: 1.22.1
Release: 1%{?dist}.harbottle
Summary: A smarter Dockerfile linter
Group:   Applications/System
License: GPL-3.0
Url:     https://github.com/%{name}/%{name}
Source0: %{url}/archive/v%{version}.tar.gz
Source1: %{url}/releases/download/v%{version}/%{name}-Linux-x86_64

%description
A smarter Dockerfile linter that helps you build best practice Docker images.
The linter is parsing the Dockerfile into an AST and performs rules on top of
the AST. It is standing on the shoulders of ShellCheck to lint the Bash code
inside RUN instructions.

%prep
%setup -q

%install
install -d -m 0755 $RPM_BUILD_ROOT%{_bindir}
install -m 0755 %{SOURCE1} $RPM_BUILD_ROOT%{_bindir}/%{name}

%files
%license LICENSE
%doc README.md docs/*
%{_bindir}/%{name}

%changelog
* Mon Feb 08 2021 - harbottle@room3d3.com - 1.22.1-1
  - Bump version

* Sun Feb 07 2021 - harbottle@room3d3.com - 1.22.0-1
  - Bump version

* Sun Jan 31 2021 - harbottle@room3d3.com - 1.21.0-1
  - Bump version

* Fri Jan 29 2021 - harbottle@room3d3.com - 1.20.0-1
  - Bump version

* Fri Nov 20 2020 - harbottle@room3d3.com - 1.19.0-1
  - Bump version

* Mon Oct 19 2020 - harbottle@room3d3.com - 1.18.2-1
  - Bump version

* Sun Oct 18 2020 - harbottle@room3d3.com - 1.18.1-1
  - Bump version

* Fri Jun 19 2020 - harbottle@room3d3.com - 1.18.0-2
  - Fix build
  - Tidy spec file

* Fri Jun 05 2020 - harbottle@room3d3.com - 1.18.0-1
  - Bump version

* Wed Jun 03 2020 - harbottle@room3d3.com - 1.17.7-1
  - Bump version

* Sun Apr 26 2020 - harbottle@room3d3.com - 1.17.6-1
  - Bump version

* Thu Jan 30 2020 - harbottle@room3d3.com - 1.17.5-1
  - Bump version

* Mon Jan 06 2020 - harbottle@room3d3.com - 1.17.4-1
  - Bump version

* Thu Nov 21 2019 - harbottle@room3d3.com - 1.17.3-1
  - Bump version

* Fri Sep 06 2019 - harbottle@room3d3.com - 1.17.2-1
  - Bump version

* Thu Jul 04 2019 - harbottle@room3d3.com - 1.17.1-1
  - Bump version

* Mon Apr 01 2019 - harbottle@room3d3.com - 1.16.3-1
  - Bump version

* Mon Mar 18 2019 - harbottle@room3d3.com - 1.16.2-1
  - Bump version

* Mon Mar 18 2019 - harbottle@room3d3.com - 1.16.1-1
  - Bump version

* Wed Jan 30 2019 - harbottle@room3d3.com - 1.16.0-1
  - Bump version

* Thu Jan 03 2019 - harbottle@room3d3.com - 1.15.0-1
  - Initial package
