%global git_owner FiloSottile
%global git_repo mkcert
%global git_archive_file v%{version}.tar.gz
%global git_archive_dir %{git_repo}-%{version}
%global go_namespace github.com/%{git_owner}/%{git_repo}

Name:          %{git_repo}
Version:       1.4.3
Release:       1%{?dist}.harbottle
Summary:       A simple zero-config tool to make locally trusted development certificates with any names you'd like
Group:         Applications/System
License:       BSD 3
Url:           https://github.com/%{git_owner}/%{git_repo}
Source0:       %{url}/archive/%{git_archive_file}
BuildRequires: golang nss-tools
Requires:      nss-tools

%description
mkcert is a simple tool for making locally-trusted development certificates. It
requires no configuration.

Using certificates from real certificate authorities (CAs) for development can
be dangerous or impossible (for hosts like localhost or 127.0.0.1), but
self-signed certificates cause trust errors. Managing your own CA is the best
solution, but usually involves arcane commands, specialized knowledge and manual steps.

mkcert automatically creates and installs a local CA in the system root store,
and generates locally-trusted certificates. mkcert does not automatically
configure servers to use the certificates, though, that's up to you.

%prep
%setup -q -n %{git_archive_dir}

%build
%define debug_package %{nil}
export GOPATH=$PWD
mkdir -p src/%{go_namespace}/
shopt -s extglob dotglob
mv !(src) src/%{go_namespace}/
shopt -u extglob dotglob
pushd src/%{go_namespace}/
rm -f go.sum
mkdir -p pkg/version/
echo %{version} > pkg/version/VERSION
go build
popd

%install
install -d -m 755 $RPM_BUILD_ROOT%{_bindir}
install -m 755 src/%{go_namespace}/%{name} $RPM_BUILD_ROOT%{_bindir}

%files
%license src/%{go_namespace}/LICENSE
%doc src/%{go_namespace}/{AUTHORS,README.md}
%{_bindir}/%{name}

%changelog
* Wed Nov 25 2020 - harbottle@room3d3.com - 1.4.3-1
  - Bump version

* Mon Oct 26 2020 - harbottle@room3d3.com - 1.4.2-1
  - Bump version

* Sun Nov 10 2019 - harbottle@room3d3.com - 1.4.1-1
  - Bump version

* Fri Aug 16 2019 - harbottle@room3d3.com - 1.4.0-1
  - Bump version

* Sun Feb 03 2019 - harbottle@room3d3.com - 1.3.0-1
  - Bump version

* Wed Jan 09 2019 - harbottle@room3d3.com - 1.2.0-1
  - Initial package
