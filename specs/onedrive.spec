Name:             onedrive
Version:          2.4.10
Release:          1%{?dist}.harbottle
Summary:          Free Client for OneDrive on Linux
Group:            Applications/System
License:          GPLv3
URL:              https://github.com/abraunegg/onedrive
Source0:          %{url}/archive/v%{version}.tar.gz
Source1:          %{name}.desktop
BuildRequires:    ldc
BuildRequires:    libcurl-devel
BuildRequires:    sqlite-devel
BuildRequires:    systemd
Requires(post):   systemd-units
Requires(preun):  systemd-units
Requires(postun): systemd-units

%description
A complete tool to interact with OneDrive on Linux. Built following the UNIX philosophy.

%prep
%setup -q %{name}-%{version}

%build
%configure
%make_build

%install
install -d -m 755 $RPM_BUILD_ROOT%{_userunitdir}
%make_install
install -d -m 755 $RPM_BUILD_ROOT%{_sysconfdir}/xdg/autostart/
install -m 644 %{SOURCE1} $RPM_BUILD_ROOT%{_sysconfdir}/xdg/autostart/
rm $RPM_BUILD_ROOT%{_prefix}/share/doc/%{name}/*

%post
%systemd_post %{name}.service

%preun
%systemd_preun %{name}.service

%postun
%systemd_postun_with_restart %{name}.service

%files
%doc CHANGELOG.md config README.md docs/*
%license LICENSE
%attr(0755,root,root) %{_bindir}/%{name}
%config(noreplace) %attr(0644,root,root) %{_sysconfdir}/logrotate.d/%{name}
%attr(0644,root,root) %{_sysconfdir}/xdg/autostart/%{name}.desktop
%{_unitdir}/%{name}.service
%{_unitdir}/%{name}@.service
%{_mandir}/*/%{name}.*

%changelog
* Fri Feb 19 2021 - harbottle@room3d3.com - 2.4.10-1
  - Bump version

* Sat Dec 26 2020 - harbottle@room3d3.com - 2.4.9-1
  - Bump version

* Mon Dec 21 2020 - harbottle@room3d3.com - 2.4.8-2
  - Build with new LDC version

* Mon Nov 30 2020 - harbottle@room3d3.com - 2.4.8-1
  - Bump version

* Mon Nov 09 2020 - harbottle@room3d3.com - 2.4.7-1
  - Bump version

* Sun Oct 04 2020 - harbottle@room3d3.com - 2.4.6-1
  - Bump version

* Thu Aug 13 2020 - harbottle@room3d3.com - 2.4.5-1
  - Bump version

* Mon Aug 10 2020 - harbottle@room3d3.com - 2.4.4-1
  - Bump version

* Fri Jul 24 2020 - harbottle@room3d3.com - 2.4.3-2
  - Build with new LDC version

* Sun Jun 28 2020 - harbottle@room3d3.com - 2.4.3-1
  - Bump version

* Tue May 26 2020 - harbottle@room3d3.com - 2.4.2-1
  - Bump version

* Fri May 22 2020 - harbottle@room3d3.com - 2.4.1-2
  - Build with new LDC version

* Fri May 01 2020 - harbottle@room3d3.com - 2.4.1-1
  - Bump version

* Sun Mar 22 2020 - harbottle@room3d3.com - 2.4.0-1
  - Bump version

* Sun Mar 22 2020 - harbottle@room3d3.com - 2.3.13-3
  - Build with new LDC version

* Sat Feb 29 2020 - harbottle@room3d3.com - 2.3.13-2
  - Build with new LDC version

* Tue Dec 31 2019 - harbottle@room3d3.com - 2.3.13-1
  - Bump version

* Mon Dec 30 2019 - harbottle@room3d3.com - 2.3.12-3
  - Build with new LDC version

* Mon Dec 09 2019 - harbottle@room3d3.com - 2.3.12-2
  - Build with new LDC version
  - Tidy-up spec file

* Wed Dec 04 2019 - harbottle@room3d3.com - 2.3.12-1
  - Bump version

* Tue Nov 05 2019 - harbottle@room3d3.com - 2.3.11-1
  - Bump version

* Tue Oct 01 2019 - harbottle@room3d3.com - 2.3.10-1
  - Bump version

* Sat Aug 31 2019 - harbottle@room3d3.com - 2.3.9-1
  - Bump version

* Sat Aug 03 2019 - harbottle@room3d3.com - 2.3.8-1
  - Bump version

* Wed Jul 03 2019 - harbottle@room3d3.com - 2.3.7-1
  - Bump version

* Tue Jul 02 2019 - harbottle@room3d3.com - 2.3.6-1
  - Bump version

* Wed Jun 19 2019 - harbottle@room3d3.com - 2.3.5-1
  - Remove log dir (yet more upstream changes)
  - Bump version

* Thu Jun 13 2019 - harbottle@room3d3.com - 2.3.4-1
  - Use configure script (fix build again)
  - Bump version

* Tue Apr 16 2019 - harbottle@room3d3.com - 2.3.3-1
  - Bump version

* Mon Apr 01 2019 - harbottle@room3d3.com - 2.3.2-1
  - Bump version

* Tue Mar 26 2019 - harbottle@room3d3.com - 2.3.1-1
  - Bump version

* Mon Mar 25 2019 - harbottle@room3d3.com - 2.3.0-1
  - Bump version
  - Fix for new Makefile

* Tue Mar 12 2019 - harbottle@room3d3.com - 2.2.6-1
  - Bump version
  - Fix for new Makefile

* Fri Jan 18 2019 - harbottle@room3d3.com - 2.2.5-1
  - Bump version

* Wed Jan 02 2019 - harbottle@room3d3.com - 2.2.4-1
  - Bump version

* Tue Aug 14 2018 Richard Grainger <grainger@gmail.com> - 2.1.1.20180814gitb8fea96-1.el7.harbottle
- Bump version

* Fri Aug 10 2018 Richard Grainger <grainger@gmail.com> - 2.1.0.20180810git0f96aef-1.el7.harbottle
- Bump version

* Mon Aug 06 2018 Richard Grainger <grainger@gmail.com> - 2.0.2.20180716gitd0209d8-1.el7.harbottle
- Bump version

* Fri Jul 13 2018 Richard Grainger <grainger@gmail.com> - 2.0.1.20180710gitf3f216c-1.el7.harbottle
- Bump version

* Fri Jun 22 2018 Richard Grainger <grainger@gmail.com> - 1.1.2.20180619git54fcb63-2.el7.harbottle
- Add xdg autostart file

* Wed Jun 20 2018 Richard Grainger <grainger@gmail.com> - 1.1.2.20180619git54fcb63-1.el7.harbottle
- Change upstream to abraunegg fork and fiddle with new Makefile
- Bump version to 1.1.2
- Update summary and description

* Tue Jun 05 2018 Richard Grainger <grainger@gmail.com> - 1.1.1.20180218gitc231b13-2.el7.harbottle
- Recompile with new version of ldc

* Sat Mar 03 2018 Richard Grainger <grainger@gmail.com> - 1.1.1.20180218gitc231b13-1.el7.harbottle
- Changes required for CentOS 7
- Include latest fixes from upstream

* Tue Jan 09 2018 Richard Grainger <grainger@gmail.com> - 1.0.1.20180106gitc7e0930-1.el7.harbottle
- Changes required for CentOS 7
- Include latest fixes from upstream

* Thu Nov 30 2017 Zamir SUN <sztsian@gmail.com> 1.0.1-1
- Update to upstream release version 1.0.1

* Tue Oct 25 2016 mosquito <sensor.wen@gmail.com> 0.1.1-2.giteb8d0fe
- add BReq systemd

* Thu Oct 20 2016 Zamir SUN <sztsian@gmail.com> 0.1.1-1.giteb8d0fe
- initial package
