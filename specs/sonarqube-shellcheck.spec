%define __jar_repack 0

Name:     sonarqube-shellcheck
Version:  2.4.0
Release:  1%{?dist}.harbottle
Summary:  SonarQube shell script plugin
Group:    Applications/System
License:  Apache-2.0
URL:      https://github.com/sbaudoin/sonar-shellcheck
Source0:  %{url}/releases/download/v%{version}/sonar-shellcheck-plugin-%{version}.jar
Autoprov: no

%description
Shell script plugin for SonarQube.

%install
install -d -m 755 $RPM_BUILD_ROOT%{_var}/lib/sonarqube/extensions/plugins
mv %{SOURCE0} $RPM_BUILD_ROOT%{_var}/lib/sonarqube/extensions/plugins/sonar-shellcheck-plugin-%{version}.jar

%pre
rm -f %{_var}/lib/sonarqube/extensions/plugins/sonar-shellcheck-plugin-*.jar
getent group sonarqube >/dev/null || groupadd -f -r sonarqube
getent passwd sonarqube >/dev/null || useradd -r -g sonarqube -d /usr/share/sonarqube -s /sbin/nologin -c "SonarQube user" sonarqube
exit 0

%files
%attr(0644,sonarqube,sonarqube) %{_var}/lib/sonarqube/extensions/plugins/sonar-shellcheck-plugin-%{version}.jar

%changelog
* Mon Nov 23 2020 - harbottle@room3d3.com - 2.4.0-1
  - Bump version

* Tue Dec 10 2019 - harbottle@room3d3.com - 2.3.0-2
  - Spec file changes for el8

* Fri Dec 06 2019 - harbottle@room3d3.com - 2.3.0-1
  - Initial packaging
