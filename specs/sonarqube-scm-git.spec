%define __jar_repack 0

Name:     sonarqube-scm-git
Version:  1.12.1.2064
Release:  1%{?dist}.harbottle
Summary:  SonarQube Git SCM plugin
Group:    Applications/System
License:  LGPL-3.0
Source0:  https://binaries.sonarsource.com/Distribution/sonar-scm-git-plugin/sonar-scm-git-plugin-%{version}.jar
Autoprov: no

%description
Git SCM plugin for SonarQube.

%prep

%install
install -d -m 755 $RPM_BUILD_ROOT%{_var}/lib/sonarqube/extensions/plugins
mv %{SOURCE0} $RPM_BUILD_ROOT%{_var}/lib/sonarqube/extensions/plugins

%pre
rm -f %{_var}/lib/sonarqube/extensions/plugins/sonar-scm-git-plugin-*.jar
getent group sonarqube >/dev/null || groupadd -f -r sonarqube
getent passwd sonarqube >/dev/null || useradd -r -g sonarqube -d /usr/share/sonarqube -s /sbin/nologin -c "SonarQube user" sonarqube
exit 0

%files
%attr(0644,sonarqube,sonarqube) %{_var}/lib/sonarqube/extensions/plugins/sonar-scm-git-plugin-%{version}.jar

%changelog
* Thu Oct 22 2020 - harbottle@room3d3.com - 1.12.1.2064-1
  - Bump version

* Mon Jun 29 2020 - harbottle@room3d3.com - 1.12.0.2034-1
  - Bump version

* Mon Apr 27 2020 - harbottle@room3d3.com - 1.11.1.2008-1
  - Bump version

* Wed Mar 11 2020 - harbottle@room3d3.com - 1.11.0.11-1
  - Bump version

* Tue Dec 10 2019 - harbottle@room3d3.com - 1.10.0.1898-2
  - Spec file changes for el8

* Tue Nov 26 2019 - harbottle@room3d3.com - 1.10.0.1898-1
  - Bump version

* Mon Sep 16 2019 - harbottle@room3d3.com - 1.9.1.1834-1
  - Bump version

* Sun Jul 21 2019 - harbottle@room3d3.com - 1.9.0.1725-2
  - Fix plugin ownership

* Sun Jul 21 2019 - harbottle@room3d3.com - 1.9.0.1725-1
  - Bump version

* Sun Jul 21 2019 - harbottle@room3d3.com - 1.8.0.1574-1
  - Initial package
