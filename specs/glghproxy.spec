%global git_rev 31b384edae5a6fcf1226c6a218c8ab6a8cd37975

Name:             glghproxy
Version:          20181003git31b384e
Release:          2.el7.harbottle
Summary:          Proxy to provide GitHub-like API on top of Gitlab.
License:          Unknown
URL:              https://github.com/dka23/gitlab-github-proxy
Source0:          https://github.com/dka23/gitlab-github-proxy/archive/%{git_rev}.tar.gz
Source1:          glghproxy.conf
Source2:          glghproxy-nginx.conf
Source3:          random.properties
Source4:          glghproxy.service
BuildRequires:    maven
BuildRequires:    systemd-units
Provides:         glghproxy
Requires:         java >= 1:1.7.0
Requires(pre):    shadow-utils
Requires(post):   systemd-units
Requires(preun):  systemd-units
Requires(postun): systemd-units

%description
Proxy to provide GitHub-like API on top of Gitlab. Especially designed to use
the JIRA DVCS connector with Gitlab.

%prep
%setup -qn gitlab-github-proxy-%{git_rev}

%build
mvn -f pom.xml clean install spring-boot:repackage

%install
install -d -m 755 $RPM_BUILD_ROOT%{_usr}
install -d -m 755 $RPM_BUILD_ROOT%{_usr}/share
install -d -m 755 $RPM_BUILD_ROOT%{_usr}/share/%{name}
install -d -m 755 $RPM_BUILD_ROOT%{_sysconfdir}
install -d -m 755 $RPM_BUILD_ROOT%{_sysconfdir}/%{name}
install -d -m 755 $RPM_BUILD_ROOT%{_unitdir}
mv target/%{name}*.war $RPM_BUILD_ROOT%{_usr}/share/%{name}/%{name}.war
install -m 644 %{SOURCE1} $RPM_BUILD_ROOT%{_sysconfdir}/%{name}/%{name}.conf
install -m 644 %{SOURCE2} $RPM_BUILD_ROOT%{_sysconfdir}/%{name}/%{name}-nginx.conf
install -m 644 %{SOURCE3} $RPM_BUILD_ROOT%{_sysconfdir}/%{name}/random.properties
install -m 644 %{SOURCE4} $RPM_BUILD_ROOT%{_unitdir}/%{name}.service

%pre
getent group %{name} >/dev/null || groupadd -f -r %{name}
getent passwd %{name} >/dev/null || useradd -r -g %{name} -d %{_usr}/share/%{name} -s /sbin/nologin -c "glghproxy user" %{name}
exit 0

%post
%systemd_post %{name}.service

%preun
%systemd_preun %{name}.service

%postun
%systemd_postun_with_restart %{name}.service

%files
%{_usr}/share
%attr(0770,root,glghproxy) %dir %{_usr}/share/%{name}
%{_usr}/share/%{name}/%{name}.war
%dir %{_sysconfdir}/%{name}
%config(noreplace) %{_sysconfdir}/%{name}/*
%{_unitdir}/%{name}.service
%doc README.md

%changelog
* Sat Jan 26 2019 - harbottle@room3d3.com - 20181003git31b384e-2
  - Add harbottle to release

* Mon Oct 15 2018 <grainger@gmail.com>
  - Bump version
* Thu Aug 31 2017 <grainger@gmail.com>
  - Update to 20170801git185a0ca
* Fri Jun 16 2017 grainger@gmail.com
  - Initial packaging
