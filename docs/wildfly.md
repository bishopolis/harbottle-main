# wildfly

[WildFly](http://wildfly.org) package in the
[harbottle-main](https://harbottle.gitlab.io/harbottle-main) CentOS repo.

## Suggested Basic Installation Procedure for CentOS 8

The following script installs the latest version of WildFly. It enables and
starts the service, running WildFly in single JVM standalone mode.

After running this script, WilfFly is available at http://localhost:8080

```bash
#!/bin/bash

# Install WildFly using harbottle-main repo
dnf -y install https://harbottle.gitlab.io/harbottle-main/8/x86_64/harbottle-main-release.rpm
dnf -y install wildfly

# Enable and start WildFly
systemctl enable --now wildfly
```

## Suggested Basic Installation Procedure for CentOS 7

The following script installs the latest version of WildFly. It enables and
starts the service, running WildFly in single JVM standalone mode.

After running this script, WilfFly is available at http://localhost:8080

```bash
#!/bin/bash

# Install WildFly using harbottle-main repo
yum -y install https://harbottle.gitlab.io/harbottle-main/7/x86_64/harbottle-main-release.rpm
yum -y install wildfly

# Enable and start WildFly
systemctl enable --now wildfly
```
