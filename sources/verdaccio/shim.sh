#!/usr/bin/env bash
export NODE_PATH=/usr/share/verdaccio/lib/node_modules
export NPM_CONFIG_PREFIX=/usr/share/verdaccio
export npm_config_prefix=/usr/share/verdaccio
exec /usr/share/verdaccio/bin/node "$@"
