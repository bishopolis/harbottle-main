# **harbottle-main**: Harbottle's main repo for Enterprise Linux

**[Browse the yum repo.](https://harbottle.gitlab.io/harbottle-main)**

A yum repo of RPM files containing various packages not available in the
standard repos. The packages are suitable for CentOS 7 and CentOS 8 (and
RHEL, Oracle Linux, etc.). Ensure you also have the
[EPEL](https://fedoraproject.org/wiki/EPEL) repo enabled.

Packages are built using GitLab CI and
[COPR](https://copr.fedorainfracloud.org/coprs/). The yum repo is hosted
courtesy of COPR and [GitLab Pages](https://pages.gitlab.io/).

## Quick Start

```bash
# Install the EPEL repo
sudo yum -y install epel-release

# Install the harbottle-main repo
# (CentOS 7)
sudo yum -y install https://harbottle.gitlab.io/harbottle-main/7/x86_64/harbottle-main-release.rpm
# (CentOS 8)
sudo yum -y install https://harbottle.gitlab.io/harbottle-main/8/x86_64/harbottle-main-release.rpm
```

After adding the repo to your system, you can install
[available packages](https://harbottle.gitlab.io/harbottle-main)
using `yum`.

In CentOS 7, this repo also contains the release RPMs for my other repos:

```bash
# Install Extra Perl Modules for Enterprise Linux repo:
yum -y install epmel-release

# Install Extra Python Packages for Enterprise Linux repo:
yum -y install epypel-release

# Install Extra Ruby Gems for Enterprise Linux repo:
yum -y install ergel-release

# Install Omnibus packages for Enterprise Linux repo:
yum -y install omnibus-release

# Install Wine 32 bit packages for Enterprise Linux repo:
yum -y install wine32-release
```

## Adding packages to the repo

[File a new issue](https://gitlab.com/harbottle/harbottle-main/issues/new).
